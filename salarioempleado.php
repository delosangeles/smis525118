<?php
 //Definición de la clase empleado
 class empleado {
 //Estableciendo las propiedades de la clase
 private static $idEmpleado = 0;
 private $nombre;
 private $apellido;
 private $isss;
 private $renta;
 private $afp;
 private $sueldoNominal;
 private $sueldoLiquido;
 private $pagoxhoraextra;
 //Declaración de constantes para los descuentos del empleado
 const descISSS = 0.05;
 const descRENTA = 0.075;
 const descAFP = 0.085;
 //Constructor de la clase
 function __construct(){
 self::$idEmpleado++;
 $this->nombre = "";
 $this->apellido = "";
 $this->sueldoLiquido = 0.0;
 $this->pagoxhoraextra = 10.0;
 }
 //Destructor de la clase
 function __destruct(){
 echo "\n<p class=\"msg\"></p>\n";
 echo "\n<p>\n<a href=\"sueldo.php\">Salario de otro empleado</a>\n</p>\n";
 }
 //Propiedades de la clase empleado
 function obtenersalario($nombre, $apellido, $salario, $horasExtras){
 $this->nombre = $nombre;
 $this->apellido = $apellido;
 $this->isss = ($salario + $horasExtras*$this->pagoxhoraextra) * self::descISSS;
 $this->renta = ($salario + $horasExtras*$this->pagoxhoraextra) * self::descRENTA;
 $this->afp = ($salario + $horasExtras*$this->pagoxhoraextra) * self::descAFP;
 $this->sueldoNominal = $salario;

 $this->sueldoLiquido = $salario + $horasExtras*$this->pagoxhoraextra - ($this->isss +
$this->renta + $this->afp);
 $this->imprimirboleta(self::$idEmpleado, $this->nombre, $this->apellido, $this->isss, $this->renta, $this->afp, $this->sueldoNominal, $horasExtras*$this-> pagoxhoraextra,
$this->sueldoLiquido);
 }

 function imprimirboleta($id, $nombre, $apellido, $descisss, $descrenta, $descafp,
$salarioNominal, $salarioHorasExtras, $salarioNeto){
   
 $tabla = "<table>\n<tr>\n";
 $tabla .= "<td>Id empleado: </td>\n";
 $tabla .= "<td>$id</td>\n</tr>\n";
 $tabla .= "<tr>\n<td>Nombre empleado: </td>\n";
 $tabla .= "<td>$nombre" . " " . "$apellido</td>\n</tr>\n";
 $tabla .= "<tr>\n<td>Salario nominal: </td>\n";
 $tabla .= "<td>$ " . number_format($salarioNominal, 2, '.', ',') . "</td>\n</tr>\n";
 $tabla .= "<tr>\n<td>Salario por horas extras: </td>\n";
 $tabla .= "<td>$ " . number_format($salarioHorasExtras, 2, '.', ',') .
"</td>\n</tr>\n";
 $tabla .= "<tr>\n<td colspan=\"2\">Descuentos</td>\n</tr>\n";
 $tabla .= "<tr>\n<td>Descuento ISSS: </td>\n";
 $tabla .= "<td>$ " . number_format($descisss, 2, '.', ',') . "</td>\n</tr>\n";
 $tabla .= "<tr>\n<td>Descuento Renta: </td>\n";
 $tabla .= "<td>$ " . number_format($descrenta, 2, '.', ',') . "</td>\n</tr>";
 $tabla .= "<tr>\n<td>Descuento AFP: </td>\n";
 $tabla .= "<td>$ " . number_format($descafp, 2, '.', ',') . "</td>\n</tr>\n";
 $tabla .= "<tr>\n<td>Total Descuentos: </td>\n";
 $tabla .= "<td>$ " . number_format($descisss + $descrenta + $descafp, 2, '.', ',') .
"</td>\n</tr>\n";
 $tabla .= "<tr>\n<td>Sueldo L&iacute;quido a Pagar: </td>\n";
 $tabla .= "<td> $" . number_format($salarioNeto, 2, '.', ',') . "</td>\n</tr>\n";
 $tabla .= "</table>\n";
 echo $tabla;
 }
 }
?> 
